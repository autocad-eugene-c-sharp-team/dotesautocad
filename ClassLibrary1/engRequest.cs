﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DotesProject
{
    public partial class engRequest : Form
    {
        public string choosedLang;

        public engRequest(List<string> langs)
        {
            InitializeComponent();
            langs.ForEach((element) => {
                comboBox1.Items.Add(element);
            });
            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.choosedLang = comboBox1.SelectedItem.ToString();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
