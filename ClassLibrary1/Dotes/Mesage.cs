﻿using DotesProject.LangugePackModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DotesProject
{
    public partial class Mesage : Form
    {
        public Color color = Color.FromArgb(255, 255, 255);
        public int point = 0;
        string title = "eror/>/";

        NoteModel langPack = LanguagePack.LangPack.Note;

        public Mesage()
        {
            InitializeComponent();
            this.pictureBox2.BackColor = color;

            // LangPack
            this.label1.Text  = this.langPack.Title;
            this.Text         = this.langPack.Name;
            this.label2.Text  = this.langPack.Attrs.Article;
            this.label4.Text  = this.langPack.Attrs.Color.Title;
            this.button3.Text = this.langPack.Attrs.Color.Name;
            this.label5.Text  = this.langPack.Attrs.Type.Title;
            this.label3.Text  = this.langPack.Attrs.Comment;
            this.button2.Text = this.langPack.Back;
            this.button1.Text = this.langPack.Create;

            this.comboBox1.Items.Clear();
            this.langPack.Attrs.Type.Types.ForEach((element) => 
            { this.comboBox1.Items.Add(element); });
            this.comboBox1.SelectedIndex = point;
        }

        public string Text_Title
        {
            get
            {
                this.title = this.Title.Text.Trim();
                foreach(char ch in title)
                {
                    if (!char.IsLetterOrDigit(ch)) {
                        MessageBox.Show(
                            caption: LanguagePack.LangPack.Exceptions.NameSymbols.Title,
                            text: LanguagePack.LangPack.Exceptions.NameSymbols.Text);
                        return "eror/>/";
                    }
                }
                return title;
            }
        }

        public string Text_text
        {
            get
            {
                return text.Text.Trim();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(Text_text) && !string.IsNullOrEmpty(Text_Title) && title != "eror/>/")
            {
                point = comboBox1.SelectedIndex;
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            color = colorDialog1.Color;
            pictureBox2.BackColor = color;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
            this.Close();
        }
    }
}
