﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using DotesProject.LangugePackModels;
using DotesProject.Layeres;
using System;
using win = System.Windows.Forms;

namespace DotesProject.Dotes
{
    class DoDote
    {
        System.Drawing.Color syscolor = System.Drawing.Color.FromArgb(255, 255, 255);
        Color autocolor = Color.FromRgb(255, 255, 255);
        int point;
        DoDoteModel langPack = LanguagePack.LangPack.DoDote;

        [CommandMethod("DoDote")]
        public void StartCreate()
        {
            
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            DotesProject.Mesage mesage = new DotesProject.Mesage();
            win.DialogResult dialogResult = Application.ShowModalDialog(mesage);
            if (dialogResult != win.DialogResult.OK) return;


            // the name of block
            syscolor = mesage.color;
            point = mesage.point;
            string blockName = mesage.Text_Title;
            string blockComent = mesage.Text_text;

            autocolor = Color.FromRgb(syscolor.R, syscolor.G, syscolor.B);

            PromptPointResult pPtRes;
            PromptPointOptions pPtOpts = new PromptPointOptions("")
            {

                // Prompt for the start point
                Message = this.langPack.CenterMes
            };
            pPtRes = doc.Editor.GetPoint(pPtOpts);
            Point3d ptStart = pPtRes.Value;

            // Exit if the user presses ESC or cancels the command
            if (pPtRes.Status == PromptStatus.Cancel) return;

            // Prompt for the end point
            pPtOpts.Message = this.langPack.RadiusMes;
            pPtOpts.UseBasePoint = true;
            pPtOpts.BasePoint = ptStart;
            pPtRes = doc.Editor.GetPoint(pPtOpts);
            Point3d ptEnd = pPtRes.Value;

            if (pPtRes.Status == PromptStatus.Cancel) return;
            Layer.Activate();

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {

                BlockTable bt = (BlockTable)tr.GetObject(db.BlockTableId, OpenMode.ForWrite);

                // At first looking that we hasn't got same name in DataBase
                if (bt.Has(blockName))
                {
                    ed.WriteMessage(LanguagePack.LangPack.Exceptions.ExistElement.Text);
                }
                else
                {

                    // create new attribution of block
                    BlockTableRecord btr = new BlockTableRecord
                    {
                        Name = blockName,
                        Comments = blockComent
                    };

                    

                    ObjectId btrId = bt.Add(btr);
                    tr.AddNewlyCreatedDBObject(btr, true);

                    switch (point)
                    {
                        case 0:
                            new Circle1(ptStart, ptEnd, btr, tr, autocolor);
                            break;
                        case 1:
                            new Circle2(ptStart, ptEnd, btr, tr, autocolor);
                            break;

                    }

                    BlockTableRecord ms = (BlockTableRecord)tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite);

                    BlockReference br = new BlockReference(Point3d.Origin, btrId);

                    ms.AppendEntity(br);
                    tr.AddNewlyCreatedDBObject(br, true);
                }
                tr.Commit();
            }
            Layer.DeActivate();
        }
    }
}
