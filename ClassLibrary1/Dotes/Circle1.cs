﻿using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using System;

namespace DotesProject.Dotes
{
    class Circle1
    {
        public Circle1(Point3d ptStart, Point3d ptEnd, BlockTableRecord btr, Transaction tr, Color autocolor)
        {
            double x, y, z, xl, yl, zl, xd, yd, zd, r,
                       x1 = ptStart.X,
                       x2 = ptEnd.X,
                       y1 = ptStart.Y,
                       y2 = ptEnd.Y,
                       z1 = ptStart.Z,
                       z2 = ptEnd.Z;
            x = x1;
            y = y1;
            z = z1;


            Circle circle = new Circle();
            circle.SetDatabaseDefaults();
            circle.Center = new Point3d(x, y, z);
            circle.Radius = r = Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (z2 - z1) + (z2 - z1) * (z2 - z1));
            circle.Color = autocolor;

            btr.AppendEntity(circle);
            tr.AddNewlyCreatedDBObject(circle, true);

            xl = x - Math.Sin(Math.PI / 4) * r;
            xd = x + Math.Sin(Math.PI / 4) * r;
            yl = y - Math.Sin(Math.PI / 4) * r;
            yd = y + Math.Sin(Math.PI / 4) * r;
            zl = z - Math.Sin(Math.PI / 4) * r;
            zd = z + Math.Sin(Math.PI / 4) * r;

            ptStart = new Point3d(xl, yd, zl);
            ptEnd = new Point3d(xd, yl, zd);

            Line line1 = new Line(ptStart, ptEnd);
            line1.Color = autocolor;
            btr.AppendEntity(line1);
            tr.AddNewlyCreatedDBObject(line1, true);

            ptStart = new Point3d(xl, yl, zl);
            ptEnd = new Point3d(xd, yd, zd);

            Line line2 = new Line(ptStart, ptEnd);
            line2.Color = autocolor;
            btr.AppendEntity(line2);
            tr.AddNewlyCreatedDBObject(line2, true);
        }
    }
}
