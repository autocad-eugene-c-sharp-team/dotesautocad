﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using DotesProject.Layeres;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace DotesProject.Dotes
{
    class Search : Form
    {
        private Panel buttonPanel = new Panel();
        private DataGridView Dotes = new DataGridView();
        private Button close = new Button();
        private Button deleteRowButton = new Button();

        public Search()
        {
            this.Load += new EventHandler(Form1_Load);
        }

        private void Form1_Load(System.Object sender, System.EventArgs e)
        {
            SetupLayout();
            SetupDataGridView();
            PopulateDataGridView();
        }

        private void songsDataGridView_CellFormatting(object sender,
            System.Windows.Forms.DataGridViewCellFormattingEventArgs e)
        {
            if (this.Dotes.Columns[e.ColumnIndex].Name == "Release Date")
            {
                if (e != null)
                {
                    if (e.Value != null)
                    {
                        try
                        {
                            e.Value = DateTime.Parse(e.Value.ToString())
                                .ToLongDateString();
                            e.FormattingApplied = true;
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("{0} is not a valid date.", e.Value.ToString());
                        }
                    }
                }
            }
        }

        private void close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void deleteRowButton_Click(object sender, EventArgs e)
        {
            //Layer layer = new Layer();
            //layer.active();
            Document doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            using (DocumentLock doclock = doc.LockDocument())
            {
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    BlockTableRecord layerTable = tr.GetObject(db.BlockTableId, OpenMode.ForWrite) as BlockTableRecord;
                    //ObjectId acLyrTblRec = this.Dotes.SelectedRows.; // дальше надо выделить id из rows и удалить объект 


                    tr.Commit();
                }
            }
            if (this.Dotes.SelectedRows.Count > 0 && this.Dotes.SelectedRows[0].Index != this.Dotes.Rows.Count - 1)
            {
                this.Dotes.Rows.RemoveAt(this.Dotes.SelectedRows[0].Index);
            }
            //layer.unactive();
        }

        private void SetupLayout()
        {
            this.Size = new Size(600, 500);

            close.Text = "Выход";
            close.Location = new Point(50, 10);
            close.Click += new EventHandler(close_Click);

            deleteRowButton.Text = "Удалить подсказку";
            deleteRowButton.Location = new Point(100, 10);
            deleteRowButton.Click += new EventHandler(deleteRowButton_Click);

            buttonPanel.Controls.Add(deleteRowButton);
            buttonPanel.Height = 50;
            buttonPanel.Dock = DockStyle.Bottom;

            this.Controls.Add(this.buttonPanel);
        }

        private void SetupDataGridView()
        {
            this.Controls.Add(Dotes);

            Dotes.ColumnCount = 3;

            Dotes.ColumnHeadersDefaultCellStyle.BackColor = Color.Navy;
            Dotes.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            Dotes.ColumnHeadersDefaultCellStyle.Font =
                new System.Drawing.Font(Dotes.Font, FontStyle.Bold);

            Dotes.Name = "TableOfTheDotes";
            Dotes.Location = new Point(8, 8);
            Dotes.Size = new Size(500, 250);
            Dotes.AutoSizeRowsMode =
                DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            Dotes.ColumnHeadersBorderStyle =
                DataGridViewHeaderBorderStyle.Single;
            Dotes.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            Dotes.GridColor = Color.Black;
            Dotes.RowHeadersVisible = false;

            Dotes.Columns[0].Name = "Заголовок";
            Dotes.Columns[1].Name = "Текст";
            Dotes.Columns[2].Name = "Id";


            Dotes.SelectionMode =
                DataGridViewSelectionMode.FullRowSelect;
            Dotes.MultiSelect = false;
            Dotes.Dock = DockStyle.Fill;

            Dotes.CellFormatting += new
                DataGridViewCellFormattingEventHandler(
                songsDataGridView_CellFormatting);
        }

        private void PopulateDataGridView()
        {


            //Layer layer = new Layer();
            //layer.active();
            // получаем БД и Editor текущего документа
            Document doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            // создаем переменную, в которой будут содержаться данные для фильтра
            TypedValue[] filterlist = new TypedValue[2];

       
            filterlist[0] = new TypedValue((int)DxfCode.Start, "INSERT");
            filterlist[1] = new TypedValue((int)DxfCode.LayerName, "DoteProject_Layer");

            // создаем фильтр
            SelectionFilter filter = new SelectionFilter(filterlist);

            // пытаемся получить ссылки на объекты с учетом фильтра
            // ВНИМАНИЕ! Нужно проверить работоспособность метода с замороженными и заблокированными слоями!
            PromptSelectionResult selRes = ed.SelectAll(filter);

            // если произошла ошибка - сообщаем о ней
            if (selRes.Status != PromptStatus.OK)
            {
                ed.WriteMessage("\nError!\n");
                return;
            }
            SelectionSet selSet = selRes.Value;
            ObjectId[] idArray = selSet.GetObjectIds();
            foreach (ObjectId blkId in idArray)
            {

                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    BlockReference blkRef = (BlockReference)tr.GetObject(blkId, OpenMode.ForRead);
                    using (BlockTableRecord btr = (BlockTableRecord)tr.GetObject(blkRef.BlockTableRecord, OpenMode.ForRead))
                    {
                        string[] list = { btr.Name, btr.Comments, Convert.ToString(btr.Id) };
                        Dotes.Rows.Add(list);
                    }
                }
            }
            Dotes.Columns[0].DisplayIndex = 0;
            Dotes.Columns[1].DisplayIndex = 1;
            Dotes.Columns[2].DisplayIndex = 2;
            //layer.unactive();
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Search
            // 
            this.ClientSize = new System.Drawing.Size(610, 455);
            this.Name = "Search";
            this.ResumeLayout(false);

        }
    }
}