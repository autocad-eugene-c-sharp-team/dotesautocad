﻿using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotesProject.Dotes
{
    class Circle2
    {
        public Circle2(Point3d ptStart, Point3d ptEnd, BlockTableRecord btr, Transaction tr, Color autocolor)
        {
            double x, y, z, r,
                       x1 = ptStart.X,
                       x2 = ptEnd.X,
                       y1 = ptStart.Y,
                       y2 = ptEnd.Y,
                       z1 = ptStart.Z,
                       z2 = ptEnd.Z;
            x = x1;
            y = y1;
            z = z1;


            Circle circle = new Circle();
            circle.SetDatabaseDefaults();
            circle.Center = new Point3d(x, y, z);
            circle.Radius = r = Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (z2 - z1) + (z2 - z1) * (z2 - z1));
            circle.Color = autocolor;

            btr.AppendEntity(circle);
            tr.AddNewlyCreatedDBObject(circle, true);

            ObjectIdCollection acObjIdColl = new ObjectIdCollection();
            acObjIdColl.Add(circle.ObjectId);

            Hatch acHatch = new Hatch();


            btr.AppendEntity(acHatch);
            tr.AddNewlyCreatedDBObject(acHatch, true);


            acHatch.SetDatabaseDefaults();
            acHatch.SetHatchPattern(HatchPatternType.PreDefined, "SOLID");
            acHatch.Color = autocolor;
            acHatch.Associative = true;
            acHatch.AppendLoop(HatchLoopTypes.Outermost, acObjIdColl);
            acHatch.EvaluateHatch(true);
        }
    }
}