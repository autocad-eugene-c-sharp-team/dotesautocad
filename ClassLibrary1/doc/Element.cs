﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotesProject.doc
{
    class Element
    {
        public Point3d Cord { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }

        
    } 
}
