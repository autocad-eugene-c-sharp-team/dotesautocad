﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Geometry;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace DotesProject.doc
{
    class XMLwork
    {
        public List<Element> elementes;
        public XmlDocument xdoc = new XmlDocument();
        private string NameOfProgect;


        public void OpenXml()
        {
            Document adoc = Application.DocumentManager.MdiActiveDocument;
            NameOfProgect = adoc.Name;

            try {
                xdoc.Load(NameOfProgect + ".xml");
                XmlElement xElementes = xdoc.DocumentElement;
                foreach (XmlNode xElement in xElementes.ChildNodes) {

                    double x = 0, y = 0, z = 0;
                    string title = "", text = "";

                    foreach (XmlNode point in xElement.ChildNodes)
                    {
                        if (point.Name == "x") x = Convert.ToDouble(point.InnerText);
                        if (point.Name == "y") y = Convert.ToDouble(point.InnerText);
                        if (point.Name == "z") z = Convert.ToDouble(point.InnerText);
                        if (point.Name == "title") title = point.InnerText;
                        if (point.Name == "text") text = point.InnerText;
                    }

                    Point3d point3D = new Point3d(x, y, z);

                    Element element = new Element()
                    {
                        Title = title,
                        Cord = point3D,
                        Text = text
                    };

                    elementes.Add(element);
                }

            }
            catch
            {
                File.Create(NameOfProgect + ".xml");
                xdoc.Load(NameOfProgect + ".xml");
                XmlDeclaration xdec = xdoc.CreateXmlDeclaration( "1.0", null, null);
                XmlElement xmlElement = xdoc.CreateElement("pointes");
                xdoc.AppendChild(xmlElement);
                xdoc.InsertBefore(xdec, xmlElement);
                xdoc.Save(NameOfProgect + ".xml");
            }
        }

        public void CloseXml(List<Element> elementes)
        {
            this.elementes = elementes;
            XmlElement xmlElement = xdoc.DocumentElement;
            xmlElement.RemoveAll();

            foreach(Element element in elementes)
            {
                XmlElement x = xdoc.CreateElement("x");
                XmlElement y = xdoc.CreateElement("y");
                XmlElement z = xdoc.CreateElement("z");

                XmlElement title = xdoc.CreateElement("title");
                XmlElement text = xdoc.CreateElement("text");
                XmlElement point = xdoc.CreateElement("point");

                string xi = Convert.ToString(element.Cord.X);
                string yi = Convert.ToString(element.Cord.Y);
                string zi = Convert.ToString(element.Cord.Z);

                XmlText xt = xdoc.CreateTextNode(xi);
                XmlText yt = xdoc.CreateTextNode(yi);
                XmlText zt = xdoc.CreateTextNode(zi);

                XmlText textt = xdoc.CreateTextNode(element.Text);
                XmlText titlet = xdoc.CreateTextNode(element.Title);

                x.AppendChild(xt);
                y.AppendChild(yt);
                z.AppendChild(zt);
                title.AppendChild(titlet);
                text.AppendChild(textt);

                point.AppendChild(x);
                point.AppendChild(y);
                point.AppendChild(z);
                point.AppendChild(title);
                point.AppendChild(text);
                xmlElement.AppendChild(point);

                xdoc.Save(NameOfProgect + ".xml");
            }
        }
    }
}
