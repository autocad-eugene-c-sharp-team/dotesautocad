﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;

using Autodesk.AutoCAD.DatabaseServices;
using System;
using DotesProject.Layeres;
using win = System.Windows.Forms;

[assembly: CommandClass(typeof(DotesProject.Ribbon1))]
[assembly: CommandClass(typeof(DotesProject.Dotes.DoDote))]

namespace DotesProject
{

    public class InitClass : IExtensionApplication
    {
        // init function (run when user load dll);
        public void Initialize()
        {
            while (LanguagePack.LangPack == null)
            {
                var listOfLangs = LanguagePack.GetLanguges();
                if (listOfLangs.Count == 0)
                {
                    win.MessageBox.Show("There aren't any languages on language directory!", "Error", win.MessageBoxButtons.OK, win.MessageBoxIcon.Error);
                    Terminate();
                    return;
                }
                var chooseLang = new engRequest(listOfLangs);
                win.DialogResult dialogResult = Application.ShowModalDialog(chooseLang);
                if (dialogResult == win.DialogResult.OK){
                    LanguagePack.ReadLangugePack(chooseLang.choosedLang);
                    if (LanguagePack.LangPack == null)
                        win.MessageBox.Show("This languge pack is unsupported!", "Error", win.MessageBoxButtons.OK, win.MessageBoxIcon.Error);
                }
                else
                    win.MessageBox.Show("Please use languge to start the program!", "Error", win.MessageBoxButtons.OK, win.MessageBoxIcon.Error);
            }
            Layer layer = new Layer();
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Editor editor = doc.Editor;
            Ribbon1 rebook = new Ribbon1();
        }

        // terminate function (run when user terminate dll);
        public void Terminate()
        {
                    
        }
    }
}