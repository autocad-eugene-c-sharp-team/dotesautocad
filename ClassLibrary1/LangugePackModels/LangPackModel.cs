﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.RepresentationModel;

namespace DotesProject.LangugePackModels
{
    class LangPackModel: BaseModel
    {
        public readonly VersionModel Versions;
        public readonly RibbonModel Ribbon;
        public readonly NoteModel Note;
        public readonly DoDoteModel DoDote;
        public readonly ExceptionsModel Exceptions;

        public LangPackModel(YamlMappingNode yamlModel)
        {
            try
            {
                this.Versions = new VersionModel(yamlModel.Children[new YamlScalarNode("Versions")]);
                this.Ribbon = new RibbonModel(yamlModel.Children[new YamlScalarNode("Ribbon")]);
                this.Note = new NoteModel(yamlModel.Children[new YamlScalarNode("Note")]);
                this.DoDote = new DoDoteModel(yamlModel.Children[new YamlScalarNode("DoDote")]);
                this.Exceptions = new ExceptionsModel(yamlModel.Children[new YamlScalarNode("Exceptions")]);
            }
            catch
            {
                this.IsException = true;
            }
            if (this.Versions.IsException || this.Ribbon.IsException || this.Note.IsException || this.DoDote.IsException
                || this.Exceptions.IsException)
            {
                this.IsException = true;
                return;
            }
        }
    }
}
