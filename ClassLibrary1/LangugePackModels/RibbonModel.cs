﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.RepresentationModel;

namespace DotesProject.LangugePackModels
{
    class RibbonModel: BaseModel
    {
        public readonly RibonElementModel CreateNote;
        public readonly RibonElementModel NotesBase;
        public readonly RibonElementModel Site;
        public readonly RibonElementModel Element4;
        public readonly RibonElementModel About;
        public readonly string Title;

        public RibbonModel(YamlNode yamlModel)
        {
            try
            {
                this.CreateNote = new RibonElementModel((yamlModel as YamlMappingNode).Children[new YamlScalarNode("CreateNote")]);
                this.NotesBase = new RibonElementModel((yamlModel as YamlMappingNode).Children[new YamlScalarNode("NotesBase")]);
                this.Site = new RibonElementModel((yamlModel as YamlMappingNode).Children[new YamlScalarNode("Site")]);
                this.Element4 = new RibonElementModel((yamlModel as YamlMappingNode).Children[new YamlScalarNode("Element4")]);
                this.About = new RibonElementModel((yamlModel as YamlMappingNode).Children[new YamlScalarNode("About")]);
                this.Title = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("Title")].ToString();
            }
            catch
            {
                this.IsException = true;
            }
            if(this.CreateNote.IsException || this.NotesBase.IsException || this.Site.IsException || this.Element4.IsException
                || this.About.IsException)
            {
                this.IsException = true;
            }
        }
    }

    class RibonElementModel: BaseModel
    {
        public readonly string Name;
        public readonly string Title;
        public readonly string Content;

        public RibonElementModel(YamlNode yamlModel)
        {
            try
            {
                this.Name = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("Name")].ToString();
                this.Title = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("Title")].ToString();
                this.Content = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("Content")].ToString();
            }
            catch
            {
                this.IsException = true;
            }
        }
    }
}
