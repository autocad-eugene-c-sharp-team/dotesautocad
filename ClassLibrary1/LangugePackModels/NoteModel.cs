﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.RepresentationModel;

namespace DotesProject.LangugePackModels
{
    class NoteModel : BaseModel
    {
        public readonly string Title;
        public readonly string Name;
        public readonly string Create;
        public readonly AttrsModel Attrs;
        public readonly string Back;

        public NoteModel(YamlNode yamlModel)
        {
            try
            {
                this.Name = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("Name")].ToString();
                this.Title = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("Title")].ToString();
                this.Attrs = new AttrsModel((yamlModel as YamlMappingNode).Children[new YamlScalarNode("Attrs")]);
                this.Create = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("Create")].ToString();
                this.Back = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("Back")].ToString();
            }
            catch
            {
                this.IsException = true;
            }
            if (this.Attrs.IsException)
            {
                this.IsException = true;
                return;
            }
        }
    }

    class AttrsModel : BaseModel
    {
        public readonly string Article;
        public readonly TypeModel Type;
        public readonly ColorModel Color;
        public readonly string Comment;

        public AttrsModel(YamlNode yamlModel)
        {
            try
            {
                this.Type = new TypeModel((yamlModel as YamlMappingNode).Children[new YamlScalarNode("Type")]);
                this.Color = new ColorModel((yamlModel as YamlMappingNode).Children[new YamlScalarNode("Color")]);
                this.Article = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("Article")].ToString();
                this.Comment = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("Comment")].ToString();
            }
            catch
            {
                this.IsException = true;
            }
            if (this.Type.IsException || this.Color.IsException)
            {
                this.IsException = true;
                return;
            }
        }
    }

    class TypeModel : BaseModel
    {
        public readonly string Title;
        public readonly List<string> Types = new List<string>();

        public TypeModel(YamlNode yamlModel)
        {
            try
            {
                this.Title = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("Title")].ToString();

                var items = (YamlSequenceNode)(yamlModel as YamlMappingNode).Children[new YamlScalarNode("Types")];
                foreach (YamlScalarNode item in items.Children)
                {
                    this.Types.Add(item.Value);

                }
            }
            catch
            {
                this.IsException = true;
            }
        }
    }

    class ColorModel : BaseModel
    {
        public readonly string Title;
        public readonly string Name;

        public ColorModel(YamlNode yamlModel)
        {
            try
            {
                this.Title = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("Title")].ToString();
                this.Name = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("Name")].ToString();
            }
            catch
            {
                this.IsException = true;
            }
        }
    }
}
