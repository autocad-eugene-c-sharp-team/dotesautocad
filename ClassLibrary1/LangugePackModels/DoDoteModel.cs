﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.RepresentationModel;

namespace DotesProject.LangugePackModels
{
    class DoDoteModel: BaseModel
    {
        public readonly string CenterMes;
        public readonly string RadiusMes;

        public DoDoteModel(YamlNode yamlModel)
        {
            try
            {
                this.CenterMes = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("CenterMes")].ToString();
                this.RadiusMes = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("RadiusMes")].ToString();
            }
            catch
            {
                this.IsException = true;
            }
        }
    }
}
