﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.RepresentationModel;

namespace DotesProject.LangugePackModels
{
    class ExceptionsModel: BaseModel
    {
        public readonly ExceptionModel NameSymbols;
        public readonly ExceptionModel ExistElement;

        public ExceptionsModel(YamlNode yamlModel)
        {
            try
            {
                this.NameSymbols = new ExceptionModel((yamlModel as YamlMappingNode).Children[new YamlScalarNode("NameSymbols")]);
                this.ExistElement = new ExceptionModel((yamlModel as YamlMappingNode).Children[new YamlScalarNode("ExistElement")]);
            }
            catch
            {
                this.IsException = true;
            }
            if (this.NameSymbols.IsException || this.ExistElement.IsException)
            {
                this.IsException = true;
                return;
            }
        }
    }

    class ExceptionModel: BaseModel
    {
        public readonly string Title;
        public readonly string Text;

        public ExceptionModel(YamlNode yamlModel)
        {
            try
            {
                this.Title = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("Title")].ToString();
                this.Text = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("Text")].ToString();
            }
            catch
            {
                this.IsException = true;
            }
        }
    }
}
