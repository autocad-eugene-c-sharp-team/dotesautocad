﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.RepresentationModel;

namespace DotesProject.LangugePackModels
{
    class VersionModel: BaseModel
    {
        public readonly string ProgramName;
        public readonly string AutorPermissions;
        public readonly string Version;
        public readonly string CompanyName;
        public readonly string Description;

        public VersionModel(YamlNode yamlModel)
        {
            try
            {
                this.ProgramName = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("ProgramName")].ToString();
                this.AutorPermissions = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("AutorPermissions")].ToString();
                this.Version = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("Version")].ToString();
                this.CompanyName = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("CompanyName")].ToString();
                this.Description = (yamlModel as YamlMappingNode).Children[new YamlScalarNode("Description")].ToString();
            }
            catch
            {
                this.IsException = true;  
            }
        }
    }
}
