﻿
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using DotesProject.Dotes;

namespace DotesProject.Layeres
{
    class Layer
    {
        public Layer()
        {
            Document adoc = Application.DocumentManager.MdiActiveDocument;
            Database db = adoc.Database;

            using (DocumentLock docloc = adoc.LockDocument())
            {
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    LayerTable layerTable = (LayerTable)tr.GetObject(db.LayerTableId, OpenMode.ForWrite);

                    if (layerTable.Has("DoteProject_Layer") == true) return;
                    

                    LayerTableRecord layerTableRecord = new LayerTableRecord();
                    layerTableRecord.Name = "DoteProject_Layer";
                    layerTable.Add(layerTableRecord);
                    layerTableRecord.IsLocked = true;

                    tr.AddNewlyCreatedDBObject(layerTableRecord, true);


                    tr.Commit();
                }
            }
            return;
        }
        
        public static void Activate()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            using (DocumentLock doclock = doc.LockDocument())
            {
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    LayerTable layerTable = tr.GetObject(db.LayerTableId, OpenMode.ForWrite) as LayerTable;
                    LayerTableRecord acLyrTblRec = tr.GetObject(layerTable["DoteProject_Layer"], OpenMode.ForWrite) as LayerTableRecord;

                    acLyrTblRec.IsLocked = false;
                    db.Clayer = layerTable["DoteProject_Layer"];
                    tr.Commit();
                }
            }
            return;

        }
        
        public static void DeActivate()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            using (DocumentLock doclock = doc.LockDocument())
            {
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    LayerTable layerTable = tr.GetObject(db.LayerTableId, OpenMode.ForWrite) as LayerTable;
                    LayerTableRecord acLyrTblRec = tr.GetObject(layerTable["DoteProject_Layer"], OpenMode.ForWrite) as LayerTableRecord;

                    db.Clayer = layerTable["0"];
                    acLyrTblRec.IsLocked = true;
                    tr.Commit();
                }
            }
            return;

        }
    }
}