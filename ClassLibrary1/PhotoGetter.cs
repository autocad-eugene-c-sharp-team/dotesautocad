﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace DotesProject
{
    static class PhotoGetter
    {
        public static Image GetImage(string imageName)
        {
            return (Image)GetBitmap(imageName);
        }

        public static Bitmap GetBitmap(string imageName)
        {
            try
            {
                return new Bitmap(LanguagePack.DirName + @"\images\" + imageName);
            }
            catch
            {
                return new Bitmap(10, 10);
            }
        }

        public static ImageSource GetImageSource(string imageName)
        {
            return Imaging.CreateBitmapSourceFromHBitmap(GetBitmap(imageName).GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
        }
    }
}
