﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.Windows;
using DotesProject.Dotes;
using System;
using win = System.Windows.Forms;
using System.Diagnostics;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.DatabaseServices;
using DotesProject.LangugePackModels;

namespace DotesProject
{
    public class Ribbon1
    {
        private readonly RibbonModel LangPack = LanguagePack.LangPack.Ribbon;
        public Ribbon1()
        {

            // create new Ribbon Split Button
            RibbonSplitButton rs1 = new RibbonSplitButton();
            // create style for Ribbon Split Button
            rs1.ListButtonStyle = Autodesk.Private.Windows.RibbonListButtonStyle.SplitButton;
            rs1.ResizeStyle = RibbonItemResizeStyles.NoResize;
            rs1.ListStyle = RibbonSplitButtonListStyle.List;
            // create new message for Ribbon Split Button
            RibbonToolTip mes1 = new RibbonToolTip();
            // Turn of help
            mes1.IsHelpEnabled = false;
            // Create button
            RibbonButton button1 = new RibbonButton();
            button1.Id = "_button1";
            // button name
            button1.Name = LangPack.CreateNote.Name;
            // button text
            button1.Text = rs1.Text = mes1.Title = LangPack.CreateNote.Title;
            // connect photo
            button1.LargeImage = PhotoGetter.GetImageSource("circle.png");
            // show image
            rs1.ShowImage = true;
            // show text
            button1.ShowText = rs1.ShowText = true;
            // make button large size 
            rs1.Size = RibbonItemSize.Large;
            // connect click event to button
            button1.CommandHandler = new CommandHandler_Button1(1);
            // add content to flow message
            mes1.Content = LangPack.CreateNote.Content;
            // button orintation
            rs1.Orientation = System.Windows.Controls.Orientation.Horizontal;
            // connect cheet to button
            button1.ToolTip = mes1;
            // add template to Ribbon Split Button
            rs1.Items.Add(button1);
            // make button is current
            rs1.Current = button1;


            RibbonSplitButton rs2 = new RibbonSplitButton();
            rs2.ListButtonStyle = Autodesk.Private.Windows.RibbonListButtonStyle.SplitButton;
            rs2.ResizeStyle = RibbonItemResizeStyles.NoResize;
            rs2.ListStyle = RibbonSplitButtonListStyle.List;
            RibbonToolTip mes2 = new RibbonToolTip();
            mes2.IsHelpEnabled = false;
            RibbonButton button2 = new RibbonButton();
            button2.Id = "_button2";
            button2.Name = LangPack.NotesBase.Name;
            button2.Text = rs2.Text = mes2.Title = LangPack.NotesBase.Title;
            rs2.ShowImage = true;
            button2.ShowText = rs2.ShowText = true;
            rs2.Size = RibbonItemSize.Large;
            button2.LargeImage = PhotoGetter.GetImageSource("line.png");
            button2.CommandHandler = new CommandHandler_Button1(2);
            mes2.Content = LangPack.NotesBase.Content;
            rs2.Orientation = System.Windows.Controls.Orientation.Horizontal;
            button2.ToolTip = mes2;
            rs2.Items.Add(button2);
            rs2.Current = button2;


            RibbonToolTip mes3 = new RibbonToolTip();
            mes3.IsHelpEnabled = false;
            RibbonButton button3 = new RibbonButton();
            button3.Id = "_button3";
            button3.Name = LangPack.Site.Name;
            button3.Text = mes3.Title = LangPack.Site.Title;
            button3.ShowText = true;
            button3.CommandHandler = new CommandHandler_Button1(3);
            mes3.Content = LangPack.Site.Content;
            button3.ToolTip = mes3;
            

            RibbonToolTip mes4 = new RibbonToolTip();
            mes4.IsHelpEnabled = false;
            RibbonButton button4 = new RibbonButton();
            button4.Id = "_button4";
            button4.Name = LangPack.Element4.Name;
            button4.Text = mes4.Title = LangPack.Element4.Title;
            button4.ShowText = true;
            button4.CommandHandler = new CommandHandler_Button1(4);
            mes4.Content = LangPack.Element4.Content;
            button4.ToolTip = mes4;

            RibbonToolTip mes5 = new RibbonToolTip();
            mes5.IsHelpEnabled = false;
            RibbonButton button5 = new RibbonButton();
            button5.Id = "_button5";
            button5.Name = LangPack.About.Name;
            button5.Text = mes5.Title = LangPack.About.Title;
            button5.ShowText = true;
            button5.CommandHandler = new CommandHandler_Button1(5);
            mes5.Content = LangPack.About.Content;
            button5.ToolTip = mes5;


            // Create container for elements
            RibbonPanelSource rbPanelSource = new RibbonPanelSource();
            rbPanelSource.Title = LangPack.Title;
            // add container to tools
            RibbonRowPanel RowPanel1 = new RibbonRowPanel();
  
            rbPanelSource.Items.Add(rs1);
            // split for columns
            rbPanelSource.Items.Add(new RibbonSeparator());
            rbPanelSource.Items.Add(rs2);
            rbPanelSource.Items.Add(new RibbonSeparator());
            RowPanel1.Items.Add(button3);
            RowPanel1.Items.Add(new RibbonRowBreak());
            RowPanel1.Items.Add(button4);
            RowPanel1.Items.Add(new RibbonRowBreak());
            RowPanel1.Items.Add(button5);
            rbPanelSource.Items.Add(RowPanel1);

            // create panel
            RibbonPanel rbPanel = new RibbonPanel();
            // add container to panel
            rbPanel.Source = rbPanelSource;

            // create ribbon
            RibbonTab rbTab = new RibbonTab();
            rbTab.Title = LangPack.Title;
            rbTab.Id = "DOTEPROJID";
            // add panel to ribbon
            rbTab.Panels.Add(rbPanel);

            // get link for tape AutoCAD
            RibbonControl rbCtrl = ComponentManager.Ribbon;
            // add to tape ribbon
            rbCtrl.Tabs.Add(rbTab);
            // make ribbon is active
            rbTab.IsActive = true;
        }

    }
     
    public class CommandHandler_Button1 : System.Windows.Input.ICommand
    {
        public int com;
        public event EventHandler CanExecuteChanged;

        public CommandHandler_Button1(int i)
        {
            com = i;
        }

        public bool CanExecute(object param)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            
            switch (com) {
                case 1:
                    using (Transaction tr = db.TransactionManager.StartTransaction())
                    {
                        doc.SendStringToExecute("DoDote\n", true, false, true);
                    }
                    break;

                case 2:
                    Search form = new Search();
                    Application.ShowModalDialog(form);
                    break;

                case 3:
                    Process.Start("chrome.exe", "https://gitlab.com/autocad-eugene-c-sharp-team");
                    break;

                case 4:
                    var listOfLangs = LanguagePack.GetLanguges();
                    if (listOfLangs.Count == 0)
                    {
                        win.MessageBox.Show("There aren't any languages on language directory!", "Error", win.MessageBoxButtons.OK, win.MessageBoxIcon.Error);
                        return;
                    }
                    var chooseLang = new engRequest(listOfLangs);
                    win.DialogResult dialogResult = Application.ShowModalDialog(chooseLang);
                    if (dialogResult == win.DialogResult.OK)
                    {
                        LanguagePack.ReadLangugePack(chooseLang.choosedLang);
                        if (LanguagePack.LangPack == null)
                            win.MessageBox.Show("This languge pack is unsupported!", "Error", win.MessageBoxButtons.OK, win.MessageBoxIcon.Error);
                    }
                    else
                        win.MessageBox.Show("Please use languge to start the program!", "Error", win.MessageBoxButtons.OK, win.MessageBoxIcon.Error);
                    break;

                case 5:
                    Version version = new Version();
                    Application.ShowModalDialog(version);
                    break;

            }
        }
    }
}