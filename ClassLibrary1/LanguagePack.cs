﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using YamlDotNet.RepresentationModel;
using System.Reflection;
using DotesProject.LangugePackModels;

namespace DotesProject
{
    static class LanguagePack
    {
        public static LangPackModel LangPack;
        public static string DirName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

       // public static LanguagePack LanguagePack;
        public static List<string> GetLanguges()
        {
            List<string> languges = new List<string>();
            foreach (var fileName in (FileInfo[]) new DirectoryInfo(DirName + @"\languagePacks\").GetFiles("*.yml"))
            {
                languges.Add(Path.GetFileNameWithoutExtension(fileName.Name));
            }
            return languges;
        }

        public static void ReadLangugePack(string langName)
        {
            var yaml = new YamlStream();
            try
            {
                var langugePackText = File.ReadAllText(DirName + @"\languagePacks\" + langName + ".yml");
                var input = new StringReader(langugePackText);
                yaml.Load(input);
                LangPack = new LangPackModel((YamlMappingNode)((YamlMappingNode)yaml.Documents[0].RootNode).Children[new YamlScalarNode("langPack")]);
            }
            catch 
            {
                LangPack = null;  
            }
            if (LangPack.IsException)
            {
                LangPack = null;
            }
        }
    }
}
