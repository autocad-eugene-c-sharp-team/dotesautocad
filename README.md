
# Autocad information dotes
## Description
This project for people who use Autocad in work and junior level developers who can read some code and resolves. 

This is tool for engineers who want make notes about some parts on drawing. You can use this utile in your real project and this points don't be on drawing after printing. Our utile's creating for plumbers, engineers, electricians and etc. You can write some notes to dots:
1) Article
1) Text

choose style of dots:
1) Elipse with cross;
1) Fill elipse;

and choose color for this dote. If you want delete dote or cheak info about all dotes on drawing, you can use ribbon button with all dots.
## System Demands
1) Autocad 16^;
1) .NET v.4.8^;
1) Free memory on computer: 20 MB.
1) Visual Studio 2020^ *(for devs)*

###### ^ - and high.
## For developers
You can use this project for free. 

If you want connect **acad.exe** for your project, you should find this file in your autocad dir and then connect them by visual studio:
**Solution Explorer -> Solution -> acad -> Properties** 
and change Exacutable and Working Directory. Most of this you should change all references paths:
**Solution Explorer -> Solution -> DotesProject -> References**
and then  change:
1) accoremgd;
1) AcCui;
1) acdbmgd;
1) acmgd;
1) AdWindows;

All this files in root dir of autocad.
f.e. changing AcCui: Properties -> Path. 

## Start project
#### For devs
Run project by Start in Visual studio. _(for debag)_
#### For users
Run  Autocad.
#### For all
Create or open drawing and write this command `netload` and then use **DotesProject.dll**. 
## Documentation
### Commands
1) **StartCreate** - refresh ribbon;
1) **DoDote** - open seatings of new point and create new point;
